import cheerio from 'cheerio'

const WELL_KNOWN_PREFIX = {
  og: { attr: 'property' },
  fb: { attr: 'property' },
  twitter: { attr: 'name' }
}
const DEFAULT_ATTR_NAME = 'property'
const PROMPT = require('../package.json').name + '> '

function resolveAttrName(key, config) {
  let knownPrefix = Object.assign(
    WELL_KNOWN_PREFIX,
    config.open_graph ? config.open_graph.known_prefix || {} : {}
  )
  let prefix = key.split(':')[0]
  let attrName = ''
  if (!knownPrefix[prefix]) {
    console.warn(
      `
${PROMPT} Unknown prefix "${key}:", Please write it in _config.yml.
${PROMPT}                 ${'^'.repeat(prefix.length)}
Example
-------------------------
open_grpah:
  known_prefix:
    ${prefix}:
      attr: property
`
    )
    attrName = DEFAULT_ATTR_NAME
  } else {
    attrName = knownPrefix[prefix].attr || DEFAULT_ATTR_NAME
  }

  return attrName
}

/**
 * @todo unit test
 * @param hexo
 * @param options
 */
module.exports = (hexo, options) => {
  let params = Object.assign(
    hexo.config.open_graph || {},
    hexo.page.open_graph || {},
    options || {}
  )

  let classicTags = hexo.open_graph ? hexo.open_graph(options) : ''
  let $ = cheerio.load(classicTags, {})

  Object.keys(params).forEach(key => {
    if (key.indexOf(':') < 0) return
    let attrName = resolveAttrName(key, hexo.config)
    let value = params[key]
    let selector = `meta[${attrName}="${key}"]`
    if (!$(selector).attr('content', value).length) {
      $('head').append(
        cheerio
          .load(`<meta>`)('meta')
          .attr(attrName, key)
          .attr('content', value) // @todo http://ogp.me/#data_types
      )
    }
  })

  return $
    .html()
    .replace('<html><head>', '')
    .replace('</head><body></body></html>', '')
}
